package lp2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Fast_and_Perfect
 */
public class fpTrucks {
    
    static File f = new File("camioes.txt");
    static File f2 = new File("camioes2.txt");
    
    static double km[] = new double[contar(f.getAbsolutePath())];
    static String codigo[] = new String[contar(f.getAbsolutePath())];
    static String linha[] = new String[contar(f.getAbsolutePath())];
    //variavel criada apenas enquanto ponto 7 não está concluido
    static Double gva[] = new Double[contar(f.getAbsolutePath())];
   
    
    public static void main(String[] args) throws IOException {
        String nome = "";
        String file = "";
        ler1(f.getAbsolutePath());
        // gerar valores aleatorios enquanto ponto 7 não está concluido
        gva[0]=3.0;
            for (int i=1;i<=codigo.length-1;i++){
        gva[i]=gva[i-1]+2.0;
}
        if (!f.exists()) {
            f.createNewFile();
        }
        if (!f2.exists()) {
            f2.createNewFile();
        }
       Scanner ler = new Scanner(System.in);
       
        char opcao;
        do {
            System.out.println("|                     Menu                                        |");
            System.out.println("|                                                                 |");
            System.out.println("|        1- Ler por codigo de Camião                              |");
            System.out.println("|        2- Remover Camião e Atualizar quilómetros por código     |");
            System.out.println("|        3- Imprimir Semanal                                      |");
            System.out.println("|        4- Guardar Informação                                    |");
            System.out.println("|        5- Ordenar                                               |");
            System.out.println("|        6- Ler Produtividade do Ficheiro                         |");     
            System.out.println("|        7- Listar 20%                                            |");
            System.out.println("|        8- Listar Informaçao em Memoria                          |");
            System.out.println("|        9- Inserir produtividade                                 |");
            System.out.println("|        0- Exit                                                  |");
            System.out.println("|              *Escolha uma opção*                                |");
            opcao = ler.next().charAt(0);
            
            switch (opcao) {
                case '0':
                    
                    System.out.println("A guardar dados do programa, aguarde!");
                    System.out.println("");
                    System.out.println("- - - - - - - - - - - - - - - - - - -");
                    System.out.println("");
                    System.out.println("             A encerrar!             ");
                    System.exit(0);
                    //
                case '1':
                    
                    System.out.println(Camiao.mostrarInformacao(f.getAbsolutePath()));
                    break;
                    //
                case '2':
                   System.out.println("1- Remover Camião");
                    System.out.println("2- Atualizar quilómetros por código");
                    int op2 = ler.nextInt();
                    if ( op2 == 1){
                        Camiao.remover(nome, file);
                        System.out.println(Camiao.atualizar());
                    }
                    if (op2 == 2){
                        System.out.println(Camiao.atualizar());
                    }
                    break;
                    //
                 case '3':
                    ImprimirSemanal.IniciarImprimir();
                    break;
                    //
                 case '4':
                        Camiao.guardar();
                    break;
                    //
                 case '5':
                    System.out.println("1- Ordenar Alfabeticamente");
                    System.out.println("2- Ordenar Decrescentemente");
                    int op1 = ler.nextInt();
                    if ( op1 == 1){
                        Camiao.ordenarAlfabeticamente(file, op1);
                    }
                    if (op1 == 2){
                        Camiao.ordenarDecrescente(file, op1);
                    }
                    break;
                 case '6':
                     System.out.println("opção para ler");
                    break;
                 case '7':
                    System.out.println(Camiao.lista(file));
                    break;
                 case '8':
                   ImprimirSemanal.listar();
                     break;
                 case '9':
                    ficheiro.produtividadeDias();
                     break;

                default:
                    System.out.println("Opção Inválida!!!");
                    System.out.println("--ERRO--\n");
                    ler.nextLine();
            }
        } while (opcao <= '9'); 
    }
    
    public static void ler1(String f) throws FileNotFoundException, IOException {
       
        int conta = 0;
        try {
            BufferedReader in = new BufferedReader(new FileReader(f));
            String[] linha;
            while(in.ready()){
                
                linha = in.readLine().split("\\|"); // lê linha e separa dados
                
                codigo[conta] = linha[0].toString();

                km[conta] = Double.parseDouble(linha[1]);
               
                conta++;
            }
            in.close();
        } catch (IOException ex) {
        }
        for (int i = 0; i < codigo.length; i++) {
            linha[i] = codigo[i] + "|" + km[i];
        }
    }
    
    public static int contar(String f) {
        int numero = 0;
        try {
            File ler = new File(f);

            int tamanhoArquivo = (int) ler.length();
            FileInputStream entrada = new FileInputStream(ler);
            DataInputStream in = new DataInputStream(entrada);

            LineNumberReader lerLinha = new LineNumberReader(new InputStreamReader(in));
            lerLinha.skip(tamanhoArquivo);
            int numLinhas = lerLinha.getLineNumber() + 1;
            numero = numLinhas;

        } catch (IOException ex) {

        }
        return numero;

    }
        
    public static int Guardar(String dados) {  
         System.out.println("Nome do Camiao a guardar");  

                    //dados = scan.next();
                    
                    try (FileWriter fw = new FileWriter(dados + ".txt", true);  
                        BufferedWriter bw = new BufferedWriter(fw);              
                        PrintWriter out = new PrintWriter(bw)) {
                        out.close();  // fecha o PrintWriter
                        bw.close();   // fecha o BufferedWriter
                        System.out.println("Gravado com sucesso!\n"); 

                    } catch (IOException e) {
                        e.printStackTrace();    // mostra os erros
                        System.out.println("O caminho não está correto!");
                    }
        return 0;
}
}