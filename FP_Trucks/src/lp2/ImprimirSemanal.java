/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp2;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Scanner;
import static lp2.fpTrucks.codigo;
import static lp2.fpTrucks.km;
import static lp2.fpTrucks.linha;
import static lp2.fpTrucks.gva;

/**
 *
 * @author Fast_and_Perfect
 */
public class ImprimirSemanal {
private static DecimalFormat df1 = new DecimalFormat("#.##");
private static String[] dados = new String[100];
private static String[] ObjectivoKm = new String[100];
private static Double[] RealKm = new Double[100];
public static String[] categoria = {"Cisterna", "Betoneira","Frigorifico","Mercadorias"};
public static String[] categoriaSiglas = {"CI", "BT","FG","MC"};
public static String[] DiasSemana = {"Segunda", "Terça","Quarta","Quinta","Sexta","Sábado","Domingo"};
private static final Scanner scan = new Scanner(System.in);
 
public static void IniciarImprimir() { // menu imprimir
int tipoCamiao=0, semana=0, ano=0;
            System.out.println("");
            System.out.println("\t#    Imprimir     #");
            System.out.println("");
            do{
            System.out.println("# Selecione o Tipo de Camiao                                         #");
            System.out.println("[1] Cisterna - [2] Betoneira - [3] Frigorifico - [4] Mercadorias");  
             tipoCamiao = scan.nextInt();
            }while(tipoCamiao<0 || tipoCamiao>5);
do{
            System.out.println("# Digite o ano a imprimir                                        #");
             //ano = scan.nextInt();
            ano=2020; //MODO TESTES
            System.out.println("# Digite a semana a imprimir                                        #");
             //semana = scan.nextInt();
             semana=8;//MODO TESTES
            }while(semana<0 || semana>53 && ano<2020 || ano>3000);

                   carregarDados(tipoCamiao);
                   carregarKmSemanais();
                   ImprimirDados(ano,semana,tipoCamiao);

}
public static void carregarDados(int cat){ 

    int add=0,add1=0,tamanho1=0;
    String[] dadosTemp = new String[100];
    String path1 = Paths.get("").toAbsolutePath().toString();
    String path=path1+"\\camioes.txt";
                    System.out.println("iniciar leitura dos dados dos camiões ");
                     try {
                         List<String> allLines = Files.readAllLines(Paths.get(path));  // lista o conteudo do ficheiro no array
                        for (String line : allLines){     
                             dadosTemp[add] = line;
                            // System.out.println(dados[add]);
                             add++;
                         }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("leitura dos dados dos camiões com ERROS!");
                    }
                     finally {
                        System.out.println("\tleitura dos dados dos camiões carregada com sucesso");
                     }
  for(int i=0;i<dadosTemp.length;i++)
 {
     if(dadosTemp[i]!=null && dadosTemp[i]!="")
     {
         tamanho1++;
     }
 }                                       
                     
                     try {
                        for (int i=0;i<tamanho1;i++){
                            boolean Tempp=dadosTemp[i].startsWith(categoriaSiglas[cat-1]);
                                if (Tempp)
                                    {
                                        String [] vec = dadosTemp[i].split("[|]",0);
                                            double km=Double.parseDouble(vec[1]);
                                            ObjectivoKm[add1]=vec[1];
                                            dados[add1]=vec[0];
                                         System.out.println(dados[add1]+" com  objectivo de "+ObjectivoKm[add1]+"km"+"  || Posiçao "+i);
                                         add1++;
                                    }  
                         }
                    } catch (Exception e) {
                        //e.printStackTrace();
                        System.out.println("Erros na filtragem");
                    }
  }

public static void carregarKmSemanais(){ 
int tamanho=0;   
 for(int i=0;i<dados.length;i++)
 {
     if(dados[i]!=null && dados[i]!="")
     {
         RealKm[i]=0.0;
         tamanho++;
     }
 }
    for (int ij=0;ij<7;ij++)  {
    int add=0,add1=0,tamanho1=0;
    String[] dadosTemp = new String[100];
    String path1 = Paths.get("").toAbsolutePath().toString();
    String path=path1+"\\"+DiasSemana[ij]+".txt";
                    System.out.println("Carregar dados do ficheiro "+DiasSemana[ij]+"!");
                     try {
                         List<String> allLines = Files.readAllLines(Paths.get(path));  // lista o conteudo do ficheiro no array
                        for (String line : allLines){     
                             dadosTemp[add] = line;
                            // System.out.println(dados[add]);
                             add++;
                         }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Km carregados com erros");
                    }
                     finally {
                        System.out.println("\tDados do ficheiro "+DiasSemana[ij]+"! Carregados com sucesso");
                     }

 for(int i=0;i<dadosTemp.length;i++)
 {
     if(dadosTemp[i]!=null && dadosTemp[i]!="")
     {
         tamanho1++;
     }
 }
                    try {
                        for (int j=0;j<tamanho;j++){
                             for (int i=0;i<tamanho1;i++)
                             {
                                 String [] vec = dadosTemp[i].split("[|]",0);
                                        if (vec[0].equals(dados[j]))
                                            {           
                                                double realTemp=Double.parseDouble(vec[1]);
                                                RealKm[j]=RealKm[j]+realTemp;
                                                System.out.println(dados[j]+" com  "+RealKm[j]+"km acumulados");
                                            } 
                              }
                        }
                         }
                     catch (Exception e) {
                        //e.printStackTrace();
                        System.out.println("Erros na Erros na filtragem do ficheiro "+DiasSemana[ij]+"!");
                    }
                     finally {
                        System.out.println("\tKM Semanais Carregados");
                     }
  }
}
private static void ImprimirDados(int ano, int semana, int cat) {
 int tamanho=0;   
 for(int i=0;i<dados.length;i++)
 {
     if(dados[i]!=null && dados[i]!="")
     {
         tamanho++;
     }
 }
                        try (FileWriter fw = new FileWriter(ano+"-"+semana + ".txt",false);  
                        BufferedWriter bw = new BufferedWriter(fw);              
                        PrintWriter out = new PrintWriter(bw)) {
                        out.println("Mapa de Tempos dos Camiões do Tipo:"+categoria[cat-1]);
                        out.println("Camiao"+"\tObjetivo "+"\tProduçao"+"\tDesvio");
                        for (int i=0; i<tamanho; i++){
                            double desvio=RealKm[i]-(Double.parseDouble(ObjectivoKm[i]));
                            String linha = (dados[i]+"\t"+ObjectivoKm[i]+"\t"+RealKm[i]+"\t"+desvio); 
                            out.println(linha);         // escreve a linha no ficheiro

                        }
                        out.close();  // fecha o PrintWriter
                        bw.close();   // fecha o BufferedWriter
                        System.out.println("Gravado com sucesso!\n"); 

                    } catch (IOException e) {
                        //e.printStackTrace();    // mostra os erros
                        System.out.println("Devido a erros nao foi possivel guardar!");
                    }
    }

public static void listar() throws IOException {
        String temp = null;
        for (int i = 0; i < codigo.length; i++) {
            if (i == 0) {
                temp = codigo[i] + "|" + km[i];
            } 
            else {
                temp += System.getProperty("line.separator") + codigo[i] + "|" + km[i];
                }
        }
        
        System.out.println(temp);
        System.out.println("Fim Listagem");
    }
}