/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
import static lp2.fpTrucks.codigo;
import static lp2.fpTrucks.km;
import static lp2.fpTrucks.linha;
import static lp2.fpTrucks.gva;
import static lp2.fpTrucks.km;

/**
 *
 * @author ASUS
 */
public class Camiao {

    public static String mostrarInformacao(String f) throws FileNotFoundException, IOException {
        String nome;
        Scanner ler = new Scanner(System.in);
        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.println("Insira o numero do Camião");

        nome = ler.nextLine();

        int verificar = 0;
        for (int i = 0; i <= codigo.length - 1; i++) {
            if (codigo[i].equals(nome)) {
                System.out.println("O camião é " + codigo[i] + " " + km[i]);
            } else {
                verificar += 1;
            }
        }
        if (verificar == codigo.length) {
            System.out.println("Nao existe camião.");
        }
        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.println("Prima ENTER para voltar ao meu ...");
        ler.nextLine();

        return "";
    }

    public static void guardar() throws IOException {
        String temp = null;
        for (int i = 0; i < codigo.length; i++) {
            if (i == 0) {
                temp = codigo[i] + "|" + km[i];
            } else {
                temp += System.getProperty("line.separator") + codigo[i] + "|" + km[i];
            }
        }
        BufferedWriter f = null;
        try {
            String File = null;
            f = new BufferedWriter(new FileWriter("camioes.txt", false));
        } catch (IOException e) {
            System.out.println("Erro a guardar");
        }
        System.out.println(temp);
        f.write(temp);
        f.close();
        System.out.println("Guardado com sucesso.");
    }

    public static int atualizar() {
        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o código do camião a alterar");
        String codigo1 = ler.nextLine();
        double h;
        int verificar = 0;

        for (int i = 0; i < codigo.length - 1; i++) {
            if (codigo[i].equals(codigo1)) {
                System.out.println("O camião " + codigo[i] + " tem " + km[i] + " quilómetros.");
                System.out.println("A alterar quilómetros.");
                h = ler.nextDouble();
                km[i] = h;
            } else {
                verificar += 1;
            }
        }
        if (verificar == codigo.length - 1) {
            System.out.println("Este camião não existe");
        }
        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.println("Prima ENTER para voltar ao menu ...");
        ler.nextLine();
        return 0;
    }

    public static String ordenarAlfabeticamente(String f, int numero) throws FileNotFoundException, IOException {

        Scanner ler = new Scanner(System.in);
        System.out.println("---------------------------------------------------------------------------------------------");

        Arrays.sort(linha);

        for (int i = 0; i < codigo.length; i++) {
            System.out.println(linha[i]);

        }

        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.println("Prima ENTER para voltar ao meu ...");
        ler.nextLine();

        return "";
    }

    public static String ordenarDecrescente(String f, int numero) throws FileNotFoundException, IOException {

        Scanner ler = new Scanner(System.in);
        System.out.println("---------------------------------------------------------------------------------------------");

        double temp = 0;
        for (int i = 0; i < codigo.length - 1; i++) {
            for (int j = i + 1; j < codigo.length - 1; j++) {
                if (km[i] < km[j]) {
                    temp = km[i];
                    km[i] = km[j];
                    km[j] = temp;
                    codigo[i] = codigo[j];
                }
            }
        }
        for (int k = 0; k < codigo.length - 1; k++) {
            System.out.println(codigo[k] + " " + km[k]);

        }

        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.println("Prima ENTER para voltar ao menu ...");
        ler.nextLine();

        return "";
    }

    public static String lista(String f2) {
        double[] per = new double[codigo.length];

        for (int i = 0; i <= codigo.length - 1; i++) {
            per[i] = (gva[i] * 100) / (km[i] * 7);
            System.out.println("O nome é " + codigo[i] + " A percentagem é" + Math.round(per[i]));
            if (per[i] < 20.0) {
                try {

                    File ficheiro = new File(f2);
                    FileWriter escreverficheiro = new FileWriter(ficheiro, true);
                    BufferedWriter escrever1 = new BufferedWriter(escreverficheiro);
                    PrintWriter escrever2 = new PrintWriter(escrever1);

                    escrever1.write(codigo[i]);
                    escrever1.newLine();
                    escrever1.close();
                    escrever2.close();
                    escreverficheiro.close();

                } catch (IOException ex) {
                    System.out.print(ex.getMessage());

                }
            }
        }
        System.out.println("Camiões guardados com Sucesso");
        return " ";
    }
    
    
    private static String nome;
    private static cam camioes[] = new cam[80];
    

 static   Scanner ler = new Scanner(System.in);
/**
    * Método que permite remover toda informção de um camião
    * 
    * 
    */
    public static void remover(String nome, String file) throws FileNotFoundException {

        File f = new File("camioes.txt");
        FileReader fr = new FileReader(f);
        BufferedReader bfr = new BufferedReader(fr);
        int j = 0;
        cam novoArray[] = new cam[80];
        
        for (int i = 0; i < camioes.length && camioes[i] != null; i++) {
            if (!camioes[i].codigo[i].equals(nome)) {
                novoArray[j] = camioes[i];
                j++;
            } else {
                System.out.println("O Camiao" + nome + " foi eliminado com sucesso");
            }
        }

        camioes = novoArray;

    }

    private static class cam {
            private Object[] codigo;

        public cam() {
        }
    }
}


